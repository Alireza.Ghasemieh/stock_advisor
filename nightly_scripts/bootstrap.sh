#!/bin/bash
#
# Bootstraps the night batch script. Specifically this script checks out the latest code from git and then runs it.
#
# Extremely important: if this file changes, it needs to be "git pull"ed manually on the VM since this file won't be
# updated until it runs. You may also need to make the bootstrap file executable for the current user.
#
# For these reason, please make minimal changes to this file and make the necessary changes in run_pipeline.sh.
#
# See README.md for more information.
#

# This is a little bit magical. This file is sourced from both .bashrc and this file and handles setting up the pipeline
# python environment.
. ~/.stock-advisor-bashrc

if [ ! -z "$BRANCH" ]
then
    git checkout $BRANCH
fi
git pull

# This is only needed when a new requirement has been added to the pipeline
pip install -r ~/stock_advisor/requirements.txt
. ~/stock_advisor/nightly_scripts/run_pipeline.sh
