# Nightly scripts

This directory contains the files necessary to run the pipeline on a remote instance. Specifically, this pipeline runs nightly on a Google Cloud Platform (GCP) Complete Engine VM.

## Overview (dev)
At 12:00am every morning, the VM is started by a GCP Cloud Scheduler job.

At 12:05am every morning, the pipeline is started by a cron job. If the pipeline complete successfully, it shuts the instance down.

At 6:00am every morning, the VM in stopped by a GCP Cloud Scheduler job. This is meant to be a fallback in case the the script fails.

Prod has a parallel set of jobs that start at 2am.

## Prerequisites
- VM must be set up properly, including a compatible version of python. See https://gitlab.com/migrations.ml/migrations-predictor/wikis/How-To/Google-Cloud-Platform
- the following environment variables should be set with appropriate values:
```
export MIGRATIONS_BRANCH=production
export PYENV_VERSION=3.7.1
export MIGRATIONS_ACTIVE_PROFILE=prod
```
- the following commands should be run:
```
cd ~/migrations-predictor
source .env/bin/activate
```
- note that this is currently handled in .migrations-bashrc for the migrations user. The bootstrap.sh script depends on this.

## Infrastructure notes

- [Google Cloud Scheduler set up directions](https://cloud.google.com/scheduler/docs/start-and-stop-compute-engine-instances-on-a-schedule)
    - start-pipeline-instance job: starts instance using start-instance-event Cloud Function event (dev)
    - stop-pipeline-instance job: stops instance using stop-instance-event Cloud Function event (dev)
    - start-pipeline-prod-instance job: starts instance using start-instance-event Cloud Function event (prod)
    - stop-pipeline-prod-instance job: stops instance using stop-instance-event Cloud Function event (prod)

- [Crontab reference guide](https://linuxconfig.org/linux-crontab-reference-guide)
    - ```crontab -e``` run on migrations user (so jobs run under that user's profile)
    - kicks off bootstrap.sh
    - here's what the crontab looks like now. Note the shell specification is needed because of use of ```source``` to set the python environment.
```
SHELL=/bin/bash
5 4 * * * ~/migrations-predictor/utilities/nightly_scripts/bootstrap.sh >> nightly-output.txt 2>&1
```
    
- bootstrap.sh
    - checks out latest code
    - runs ```pip``` to ensure python requirements are satisfied
    - starts pipeline

- run_pipeline.sh
    - download data from Quandl and Moodys
    - pipeline: load with persist interim
    - pipeline: train, predict, upload, time series store (pd)
    - shut down instance

## Running outside of scheduled times
1. start VM  
1. ssh into VM
1. ```sudo su migrations```
1. ensure that enviroment variables are set properly and that the correct version of the code is checked out
1. ```nohup utilities/nightly_scripts/run_pipeline.sh &```

'nohup' is necessary because of the length of the pipeline - it's likely that the ssh session will end before the pipeline is complete.

Keep in mind that the automated run will start around midnight regardless of what you're doing on the machine.
