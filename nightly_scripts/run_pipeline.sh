#!/bin/bash
#
# Nightly batch job to:
# - pipeline: download, load, train, predict, upload (pd)
# - shut down
#
# See README.md for more details.
python pipeline.py -SD -FD -S -T -M -A -AA -R
sudo shutdown -h now
