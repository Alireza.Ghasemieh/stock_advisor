from copy import deepcopy
import requests
import numpy as np
import pandas as pd
from time import sleep
from pathlib import Path
from utils import save_csv
from datetime import datetime
from log_setup import get_logger
from bs4 import BeautifulSoup as bs
from configparser import ConfigParser
from pandas import DataFrame, to_datetime

logger = get_logger(__name__)


class Fundamentals_Download:
    """
    functions to get and parse data from FinViz
    """
    def __init__(self, config: ConfigParser):
        self.config = config
        self.fundamentals_downloaded_csv_path = Path(config['Data_Sources']['fundamentals csv'])

    def download(self, ticker_list: DataFrame) -> DataFrame:
        logger.info("Fundamentals is downloading")
        metric = ['Index', 'Market Cap', 'Income', 'Sales', 'Book/sh', 'Cash/sh', 'Dividend', 'Dividend %',
                  'Employees', 'Optionable', 'Shortable', 'Recom', 'P/E', 'Forward P/E', 'PEG', 'P/S', 'P/B', 'P/C',
                  'P/FCF', 'Quick Ratio', 'Current Ratio', 'Debt/Eq', 'LT Debt/Eq', 'SMA20', 'EPS (ttm)', 'EPS next Y',
                  'EPS next Q', 'EPS this Y', 'EPS next 5Y', 'EPS past 5Y', 'Sales past 5Y', 'Sales Q/Q', 'EPS Q/Q',
                  'Earnings', 'SMA50', 'Insider Own', 'Insider Trans', 'Inst Own', 'Inst Trans', 'ROA', 'ROE', 'ROI',
                  'Gross Margin', 'Oper. Margin', 'Profit Margin', 'Payout', 'SMA200', 'Shs Outstand', 'Shs Float',
                  'Short Float', 'Short Ratio', 'Target Price', '52W Range', '52W High', '52W Low', 'RSI (14)',
                  'Rel Volume', 'Avg Volume', 'Volume', 'Perf Week', 'Perf Month', 'Perf Quarter', 'Perf Half Y',
                  'Perf Year', 'Perf YTD', 'Beta', 'ATR', 'Volatility', 'Prev Close', 'Price', 'Change']
        data_df = pd.DataFrame(columns=metric)
        data_df = self.get_fundamental_data(data_df, ticker_list)
        save_csv(data_df, Path(f"{self.fundamentals_downloaded_csv_path}_{datetime.now().date()}_"
                               f"{datetime.now().hour}-{datetime.now().minute}.csv"))
        return data_df

    @staticmethod
    def fundamental_metric(soup, metric):
        if soup.find(text=metric) is None:
            return False
        else:
            return soup.find(text=metric).find_next(class_='snapshot-td2').text

    def get_fundamental_data(self, data_df, ticker_list):
        for index, item in ticker_list.iterrows():
            symbol = item['ticker']
            logger.info(f"Downloading {symbol} information.")
            try:
                url = "http://finviz.com/quote.ashx?t=" + symbol.lower()
                headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}
                soup = bs(requests.get(url, headers=headers).content, "lxml")
                for m in data_df.columns:
                    data = self.fundamental_metric(soup, m)
                    if not data:
                        logger.warning(f'{symbol} not found')
                        break
                    else:
                        data_df.loc[symbol, m] = data
            except Exception as e:
                logger.warning(e)
            sleep(7)

        for index, item in ticker_list.iterrows():
            symbol = item['ticker']
            data_df.loc[symbol, 'date'] = datetime.now().date()
        cols = list(data_df.columns)
        cols = [cols[-1]] + cols[:-1]
        data_df = data_df[cols]

        data_df['ticker'] = data_df.index
        cols = list(data_df.columns)
        cols = [cols[-1]] + cols[:-1]
        data_df = data_df[cols]
        data_df.reset_index(drop=True, inplace=True)
        data_df = self.fundamental_rename(data_df)
        data_df = self.fundamental_clean(data_df)
        data_df = self.add_primary_key(data_df)
        logger.info(f"Downloaded data size is {data_df.shape}")
        return data_df

    @staticmethod
    def fundamental_rename(data_df: DataFrame) -> DataFrame:
        data_df = data_df.rename(columns=
                                 {'Index': 'Market_Index', 'Market Cap': 'Market_Cap', 'Book/sh': 'Book_on_sh',
                                  'Cash/sh': 'Cash_on_sh', 'Dividend %': 'Dividend_p', 'P/E': 'P_on_E',
                                  'Forward P/E': 'Forward_P_on_E', 'P/S': 'P_on_S', 'P/B': 'P_on_B', 'P/C': 'P_on_C',
                                  'P/FCF': 'P_on_FCF', 'Quick Ratio': 'Quick_Ratio', 'Current Ratio': 'Current_Ratio',
                                  'Debt/Eq': 'Debt_on_Eq', 'LT Debt/Eq': 'LT_Debt_on_Eq', 'EPS (ttm)': 'EPS_ttm',
                                  'EPS next Y': 'EPS_next_Y', 'EPS next Q': 'EPS_next_Q', 'EPS this Y': 'EPS_this_Y',
                                  'EPS next 5Y': 'EPS_next_5Y', 'EPS past 5Y': 'EPS_past_5Y',
                                  'Sales past 5Y': 'Sales_past_5Y', 'Sales Q/Q': 'Sales_Q_Q', 'EPS Q/Q': 'EPS_Q_Q',
                                  'Insider Own': 'Insider_Own', 'Insider Trans': 'Insider_Trans',
                                  'Inst Own': 'Inst_Own', 'Inst Trans': 'Inst_Trans', 'Gross Margin': 'Gross_Margin',
                                  'Oper. Margin': 'Oper_Margin', 'Profit Margin': 'Profit_Margin',
                                  'Shs Outstand': 'Shs_Outstand', 'Shs Float': 'Shs_Float',
                                  'Short Float': 'Short_Float', 'Short Ratio': 'Short_Ratio',
                                  'Target Price': 'Target_Price', '52W Range': '_52W_Range', '52W High': '_52W_High',
                                  '52W Low': '_52W_Low', 'RSI (14)': 'RSI_14', 'Rel Volume': 'Rel_Volume',
                                  'Avg Volume': 'Avg_Volume', 'Perf Week': 'Perf_Week', 'Perf Month': 'Perf_Month',
                                  'Perf Quarter': 'Perf_Quarter', 'Perf Half Y': 'Perf_Half_Y',
                                  'Perf Year': 'Perf_Year', 'Perf YTD': 'Perf_YTD', 'Prev Close': 'Prev_Close',
                                  'Change': 'Change_Price'})
        return data_df

    def fundamental_clean(self, data: DataFrame) -> DataFrame:
        logger.info("Fundamentals data cleaning in progress")
        data_df = deepcopy(data)
        data_df = data_df.replace({np.nan: None, '-': None})

        data_df['Volume'] = data_df['Volume'].map(lambda x: self.str_to_int_convert(x))

        data_df['Market_Cap'] = data_df['Market_Cap'].map(lambda x: self.number_convert(x))
        data_df['Income'] = data_df['Income'].map(lambda x: self.number_convert(x))
        data_df['Sales'] = data_df['Sales'].map(lambda x: self.number_convert(x))
        data_df['Shs_Outstand'] = data_df['Shs_Outstand'].map(lambda x: self.number_convert(x))
        data_df['Shs_Float'] = data_df['Shs_Float'].map(lambda x: self.number_convert(x))
        data_df['Avg_Volume'] = data_df['Avg_Volume'].map(lambda x: self.number_convert(x))

        data_df['Dividend_p'] = data_df['Dividend_p'].map(lambda x: self.percentage_convert(x))
        data_df['SMA20'] = data_df['SMA20'].map(lambda x: self.percentage_convert(x))
        data_df['EPS_this_Y'] = data_df['EPS_this_Y'].map(lambda x: self.percentage_convert(x))
        data_df['EPS_next_5Y'] = data_df['EPS_next_5Y'].map(lambda x: self.percentage_convert(x))
        data_df['EPS_past_5Y'] = data_df['EPS_past_5Y'].map(lambda x: self.percentage_convert(x))
        data_df['Sales_past_5Y'] = data_df['Sales_past_5Y'].map(lambda x: self.percentage_convert(x))
        data_df['Sales_Q_Q'] = data_df['Sales_Q_Q'].map(lambda x: self.percentage_convert(x))
        data_df['EPS_Q_Q'] = data_df['EPS_Q_Q'].map(lambda x: self.percentage_convert(x))
        data_df['SMA50'] = data_df['SMA50'].map(lambda x: self.percentage_convert(x))
        data_df['Insider_Own'] = data_df['Insider_Own'].map(lambda x: self.percentage_convert(x))
        data_df['Insider_Trans'] = data_df['Insider_Trans'].map(lambda x: self.percentage_convert(x))
        data_df['Inst_Own'] = data_df['Inst_Own'].map(lambda x: self.percentage_convert(x))
        data_df['Inst_Trans'] = data_df['Inst_Trans'].map(lambda x: self.percentage_convert(x))
        data_df['ROA'] = data_df['ROA'].map(lambda x: self.percentage_convert(x))
        data_df['ROE'] = data_df['ROE'].map(lambda x: self.percentage_convert(x))
        data_df['ROI'] = data_df['ROI'].map(lambda x: self.percentage_convert(x))
        data_df['Gross_Margin'] = data_df['Gross_Margin'].map(lambda x: self.percentage_convert(x))
        data_df['Oper_Margin'] = data_df['Oper_Margin'].map(lambda x: self.percentage_convert(x))
        data_df['Profit_Margin'] = data_df['Profit_Margin'].map(lambda x: self.percentage_convert(x))
        data_df['Payout'] = data_df['Payout'].map(lambda x: self.percentage_convert(x))
        data_df['SMA200'] = data_df['SMA200'].map(lambda x: self.percentage_convert(x))
        data_df['Short_Float'] = data_df['Short_Float'].map(lambda x: self.percentage_convert(x))
        data_df['_52W_High'] = data_df['_52W_High'].map(lambda x: self.percentage_convert(x))
        data_df['_52W_Low'] = data_df['_52W_Low'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_Week'] = data_df['Perf_Week'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_Month'] = data_df['Perf_Month'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_Quarter'] = data_df['Perf_Quarter'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_Half_Y'] = data_df['Perf_Half_Y'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_Year'] = data_df['Perf_Year'].map(lambda x: self.percentage_convert(x))
        data_df['Perf_YTD'] = data_df['Perf_YTD'].map(lambda x: self.percentage_convert(x))
        data_df['Change_Price'] = data_df['Change_Price'].map(lambda x: self.percentage_convert(x))

        data_df = data_df.round(2)
        return data_df

    @staticmethod
    def percentage_convert(data: str) -> float:
        if data is None:
            return None
        elif data[-1] == '%':
            return float(data[:-1])
        else:
            return float(data)

    @staticmethod
    def str_to_int_convert(data: str) -> int:
        if data is None:
            return None
        else:
            return int(data.replace(',', ''))

    @staticmethod
    def number_convert(data: str) -> float:
        if data is None:
            return None
        elif data[-1] == 'K':
            return int(float(data[:-1]) * 1000)
        elif data[-1] == 'M':
            return int(float(data[:-1]) * 1000000)
        elif data[-1] == 'B':
            return int(float(data[:-1]) * 1000000000)
        else:
            return round(float(data), 2)

    @staticmethod
    def add_primary_key(data: DataFrame) -> DataFrame:
        data_df = data.copy()
        logger.info("Adding primary key")
        data_df['date'] = to_datetime(data_df['date'])
        data_df['date'] = data_df['date'].map(lambda x: x.date())
        data_df['date_str'] = data_df['date'].map(lambda x: str(x))
        data_df['id'] = data_df['ticker'] + "-" + data_df['date_str']
        cols = list(data_df.columns)
        cols = [cols[-1]] + cols[:-1]
        data_df = data_df[cols]
        data_df.drop(columns='date_str', inplace=True)
        return data_df
