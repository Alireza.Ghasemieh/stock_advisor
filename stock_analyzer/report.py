from pathlib import Path
from utils import save_csv
from log_setup import get_logger
from configparser import ConfigParser
from datetime import timedelta, datetime
from stock_analyzer.notifier import Notifier
from stock_analyzer.database.database_communication import MySQL_Connection

logger = get_logger(__name__)


class Reporter:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.decision_table_name = self.config['MySQL']['decision table name']
        self.report_message_csv_path = config['Data_Sources']['report massage path']
        self.report_message_csv_path = config['Data_Sources']['report massage path']

        self.alireza_address = config['Email']['alireza address']
        self.mehrdad_address = config['Email']['mehrdad address']

    def report_message(self, day_shift=30):
        logger.info("Report message generator started")
        mysql = MySQL_Connection(config=self.config)
        latest_date_df = mysql.select(f"SELECT max(date) FROM {self.decision_table_name};")

        if latest_date_df is not None:
            latest_date_in_db = latest_date_df.iloc[0][0]
        else:
            latest_date_in_db = datetime.strptime('2020-01-01', "%Y-%m-%d").date()

        starting_date = str(latest_date_in_db - timedelta(days=(day_shift + 3)))
        data = mysql.select(f"SELECT * FROM {self.decision_table_name} WHERE date >= '{starting_date}';")
        csv_path = f"{self.report_message_csv_path}_{datetime.now().date()}_{datetime.now().hour}-{datetime.now().minute}.csv"
        save_csv(data, Path(csv_path))

        try:
            report_subject = f'Daily Stock Report from Stock Analyzer - {datetime.now().date()}'
            report_message = f"Daily stock analyzer report."
            receiver_email = [self.alireza_address, self.mehrdad_address]
            notifier = Notifier(self.config)
            notifier.notifier(subject=report_subject, receiver_email=receiver_email, body_message=report_message,
                              attachment_path=csv_path)
        except BaseException as e:
            logger.error("Sending message error")
            raise e
