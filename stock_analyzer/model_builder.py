from copy import deepcopy
import configuration
from pathlib import Path
from utils import save_csv
from pandas import DataFrame, read_csv
from log_setup import get_logger
from configparser import ConfigParser
from datetime import timedelta, datetime
from stock_analyzer.database.database_communication import MySQL_Connection

logger = get_logger(__name__)


class Model_Builder:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.ticker_list_path = Path(config['Data_Sources']['tickers list csv'])
        self.main_table_name = self.config['MySQL']['main table name']
        self.analysis_table_name = self.config['MySQL']['analysis table name']
        self.path = config['Data_Sources']['analysis equity price csv']

    def build(self):
        None
