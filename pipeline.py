import sys
import argparse
import configuration
from typing import List
from pathlib import Path
from argparse import Namespace
from log_setup import get_logger
from argparse import ArgumentParser
from configuration import ConfigParser
from pipeline_report import PipelineReport
from stock_analyzer.model_builder import Model_Builder
from stock_analyzer.analyzer import Analyzer
from stock_analyzer.predictor import Predictor
from stock_analyzer.report import Reporter
from utils import read_csv, create_directory
from stock_analyzer.transformer import Transformer
from stock_analyzer.advanced_analyzer import Advanced_Analyzer
from stock_analyzer.database.database_insertion import DB_Insertion
from stock_analyzer.downloader.stock_data_downloader import Stock_Downloader
from stock_analyzer.downloader.fundamentals_data_downloader import Fundamentals_Download

logger = get_logger(__name__)


class Stock_Advisor_Pipeline:
    def __init__(self, config: ConfigParser):
        self.pipeline_report = PipelineReport()
        self.config = config
        self.ticker_list_path = Path(config['Data_Sources']['tickers list csv'])
        self.finviz_ticker_list = Path(config['Data_Sources']['finviz tickers list csv'])

        self.stage_table_name = self.config['MySQL']['stage table name']
        self.fundamentals_table_name = self.config['MySQL']['fundamentals table name']
        self.main_table_name = self.config['MySQL']['main table name']

        self.data_folder_path = Path(config['General']['data directory'])
        self.data_download_folder_path = Path(config['General']['data download directory'])
        self.data_transform_folder_path = Path(config['General']['data transform directory'])
        self.data_analysis_folder_path = Path(config['General']['data analysis directory'])
        self.decisions_folder_path = Path(config['General']['decisions directory'])
        self.daily_report_folder_path = Path(config['General']['daily_report directory'])
        self.temp_folder_path = Path(config['General']['temp directory'])

    def stock_advisor(self, arguments: List[str]) -> None:
        logger.info("+----------------------------------+")
        logger.info("| Stock observer pipeline started. |")
        logger.info("+----------------------------------+")

        self.pipeline_report.arguments = ' '.join(arguments)

        parser: ArgumentParser = argparse.ArgumentParser(description=__doc__)

        parser.add_argument("-SD", "--stock_download", help="download raw data files", action="store_true")
        parser.add_argument("-FD", "--fundamentals_download", help="download raw data files", action="store_true")
        parser.add_argument("-S", "--stage_db", help="download raw data files", action="store_true")
        parser.add_argument("-T", "--transform", help="download raw data files", action="store_true")
        parser.add_argument("-M", "--main_db", help="download raw data files", action="store_true")
        parser.add_argument("-A", "--analyzer", help="download raw data files", action="store_true")
        parser.add_argument("-AA", "--advanced_analyzer", help="download raw data files", action="store_true")
        parser.add_argument("-MB", "--model_builder", help="download raw data files", action="store_true")
        parser.add_argument("-P", "--predictor", help="download raw data files", action="store_true")
        parser.add_argument("-R", "--report", help="download raw data files", action="store_true")

        args: Namespace = parser.parse_args(args=arguments)

        create_directory(self.data_folder_path)
        create_directory(self.data_download_folder_path)
        create_directory(self.data_transform_folder_path)
        create_directory(self.data_analysis_folder_path)
        create_directory(self.decisions_folder_path)
        create_directory(self.daily_report_folder_path)
        create_directory(self.temp_folder_path)

        try:
            if args.stock_download:
                logger.info("************------------( Stock data downloader started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Stock Data Downloader")
                try:
                    ticker_list = read_csv(self.ticker_list_path)
                    stock = Stock_Downloader(self.config)
                    stock_price_data_df = stock.download(ticker_list=ticker_list)
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.fundamentals_download:
                logger.info("************------------( Fundamentals data downloader started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Fundamentals Data Downloader")
                try:
                    finviz_ticker_list = read_csv(self.finviz_ticker_list)
                    fundamentals = Fundamentals_Download(self.config)
                    fundamentals_data_df = fundamentals.download(ticker_list=finviz_ticker_list)
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.stage_db:
                logger.info("************------------( Database staging started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Database Stagger")
                try:
                    stage_db = DB_Insertion(self.config)
                    try:
                        stock_price_data_df
                    except NameError:
                        logger.warning("There is no stock data.")
                    else:
                        stage_db.insertion(data_df=stock_price_data_df, table_name=self.stage_table_name,
                                           table_type='stage')
                    try:
                        fundamentals_data_df
                    except NameError:
                        logger.warning("There is no fundamentals data.")
                    else:
                        stage_db.insertion(data_df=fundamentals_data_df, table_name=self.fundamentals_table_name,
                                           table_type='fundamentals')
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.transform:
                logger.info("************------------( Transformation started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Transformation")
                try:
                    transformation = Transformer(self.config)
                    processed_data_df = transformation.transform()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.main_db:
                logger.info("************------------( Main database insertion started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Main DB Insertion")
                try:
                    derivative_features = list(processed_data_df.columns)[8:]
                    main_db = DB_Insertion(self.config)
                    main_db.insertion(data_df=processed_data_df, table_name=self.main_table_name,
                                      table_type='main', derivative_features=derivative_features)
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.analyzer:
                logger.info("************------------( Analyzer started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Analyzer")
                try:
                    analyzer = Analyzer(self.config)
                    analyzer.analysis()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.advanced_analyzer:
                logger.info("************------------( Advanced analyzer started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Advanced analyzer")
                try:
                    advanced_analyzer = Advanced_Analyzer(self.config)
                    advanced_analyzer.analyze()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.model_builder:
                logger.info("************------------( Model Building started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Model Building")
                try:
                    model = Model_Builder(self.config)
                    model.build()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.predictor:
                logger.info("************------------( Predictor started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Predictor")
                try:
                    prediction = Predictor(self.config)
                    prediction.predict()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.report:
                logger.info("************------------( Reporter started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Reporter")
                try:
                    report = Reporter(self.config)
                    report.report_message(day_shift=-3)
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            self.pipeline_report.mark_success()
        except BaseException as e:
            self.pipeline_report.mark_failure()
            raise e

        finally:
            self.pipeline_report.log()


if __name__ == '__main__':
    pipeline: Stock_Advisor_Pipeline = Stock_Advisor_Pipeline(configuration.get())
    pipeline.stock_advisor(sys.argv[1:])
